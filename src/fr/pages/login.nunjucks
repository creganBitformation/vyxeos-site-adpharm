{% extends "layout.nunjucks" %} {% block title %} Connexion | Vyxeos {% endblock %} {% block content %}

<style>
    header {
        display: none;
    }

    footer {
        display: none;
    }

    .login,
    .login p {
        color: #762679;
    }

    .form-group>label {
        width: 100%;
        padding-bottom: 1rem;
        border-bottom: 3px solid white;
        margin-bottom: 1rem;
    }

    @media (max-width: 1200px) {
        .login-area {
            background-color: #FFFFFF;
        }
    }

    .arrow-left {
        left: 1em !important;
    }

    .tooltip-error {
        border-radius: 0;
        max-width: 300px;
        background-color: #FFFFFF;
        border: 1px solid #CACACA;
        color: #E60000;
    }



    .bs-tooltip-auto[x-placement^=top] .arrow::before,
    .bs-tooltip-top .arrow::before {
        border-top-color: #CACACA;
    }

    .bs-tooltip-auto[x-placement^=right] .arrow::before,
    .bs-tooltip-right .arrow::before {
        border-right-color: #CACACA;
    }

    .bs-tooltip-auto[x-placement^=bottom] .arrow::before,
    .bs-tooltip-bottom .arrow::before {
        border-bottom-color: #CACACA;
    }

    .bs-tooltip-auto[x-placement^=left] .arrow::before,
    .bs-tooltip-left .arrow::before {
        border-left-color: #CACACA;
    }

    .login .btn-success {
        text-transform: uppercase;
        background: #00BFD6;
        color: white;
        border-color: #00BFD6;
        height: 50px;
        border-radius: 25px;
    }
</style>

    <!--data-toggle="tooltip"   data-placement="bottom" title="Please pick a provider type"-->
<main class="bg-blue">
    <section id="login" class="login pt-5 pb-5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-3">

                    <img style="width:235px;" class="img my-3 d-none d-xl-block" src="/assets/img/fr/logo_login_desktop.png"
                        alt="Vyxeos logo - Desktop" />
                    <img class="img-fluid my-3 d-block mx-auto d-xl-none" src="/assets/img/fr/logo_login_mobile.png"
                        alt="Vyxeos logo - Mobile" />
                </div>
                <div class="col-12 col-lg-3 login-area">

                    <form class="login-form  mt-5" novalidate>
                        <div id="inputCareProviderType_input" class="form-group mt-3 mb-5" data-toggle="tooltip"
                                data-placement="bottom" title="Veuillez choisir un type de professionnel de la santé">
                            <label for="inputCareProviderType">Je suis un.e (veuillez choisir une réponse) :</label>

                            <div class="custom-control custom-radio">
                                <input type="radio" id="inputCareProviderType1" name="inputCareProviderType"
                                    value="physician" class="custom-control-input">
                                <label class="custom-control-label" for="inputCareProviderType1">Médecin canadien</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="inputCareProviderType2" name="inputCareProviderType"
                                    value="nurse" class="custom-control-input">
                                <label class="custom-control-label" for="inputCareProviderType2">Infirmier.ère canadien.ne</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input  type="radio" id="inputCareProviderType3" name="inputCareProviderType"
                                    value="other" class="custom-control-input"  >
                                <label class="custom-control-label" for="inputCareProviderType3">Autre, de nationalité canadienne</label>
                            </div>

                            <div class="invalid-tooltip">Veuillez choisir un type de professionnel de la santé</div>
                        </div>

                        <div class="form-group mt-3 mb-5">
                            <label for="inputProvince">Province ou territoire :</label>
                            <select class="form-control" id="inputProvince" name="inputProvince" data-toggle="tooltip"
                                data-placement="bottom" title=" Veuillez choisir une province ou un territoire" required>
                                <option value="" disabled selected>Veuillez choisir une réponse</option>
                                <option value="alberta">Alberta</option>
                                <option value="british_columbia">Colombie-Britannique</option>
                                <option value="manitoba">Manitoba</option>
                                <option value="new_brunswick">Nouveau-Brunswick</option>
                                <option value="newfoundland">Terre-Neuve-et-Labrador</option>
                                <option value="northwest_territories">Territoires du Nord-Ouest</option>
                                <option value="nova_scotia">Nouvelle-Écosse</option>
                                <option value="nunavut">Nunavut</option>
                                <option value="ontario">Ontario</option>
                                <option value="prince_edward_island">Île-du-Prince-Édouard</option>
                                <option value="quebec">Québec</option>
                                <option value="saskatchewan">Saskatchewan</option>
                                <option value="yukon">Yukon</option>
                            </select>
                            <div class="invalid-tooltip">
                                Veuillez choisir une province ou un territoire
                            </div>
                        </div>

                        <div class="form-group mt-3 mb-5">
                            <label for="inputLicenseNumber">Numéro de permis d’exercice</label>
                            <input type="text" class="form-control" id="inputLicenseNumber" name="inputLicenseNumber"
                                data-toggle="tooltip" data-placement="bottom"
                                title="Désolé, le numéro que vous avez entré n’est pas valide." placeholder="Enter number"
                                required>
                            <div class="invalid-tooltip">

                            </div>
                        </div>

                        <div class="form-group mt-3 mb-5 text-center">
                            <button type="submit" class="btn btn-lg btn-success">
                                Entrer <i class="fas fa-arrow-right"></i>
                            </button>
                        </div>

                        <p class="click-disclaimer mt-3 mb-5 text-center">

                        </p>

                    </form>
                </div>
                <div class="col-md-6 my-5  d-none d-lg-block  d-lx-block">
                    <img src="/assets/img/fr/yellow_hug.png" alt="" class="img-fluid">


                </div>
            </div>

    </section>
</main>
{% endblock %}
{% block script %}
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip({
            template: '<div class="tooltip" role="tooltip"><div class="arrow arrow-left"></div><div class="tooltip-inner tooltip-error"></div></div>',
            trigger: 'manual'
        });
        $('.login-form').submit(function (e) {
            $('#inputProvince').tooltip('hide');
            $('#inputLicenseNumber').tooltip('hide');
            $('#inputCareProviderType_input').tooltip('hide');
            e.preventDefault();
            var type = $("input[name='inputCareProviderType']:checked").val();
            var province = $('#inputProvince').find(':selected').val();
            var licenseNo = $('#inputLicenseNumber').val();

            if ( ! type ) {
                $('#inputCareProviderType_input').tooltip('show');
                return false;
            }
            if ( ! province ) {
                $('#inputProvince').tooltip('show');
                return false;
            }
            if ( ! licenseNo ) {
                $('#inputLicenseNumber').tooltip('show');
                return false;
            }
            if (licenseNo.length < 4 ) {
                $('#inputLicenseNumber').tooltip('show');
                return false;
            }
            window.localStorage.setItem('login_info', JSON.stringify( { 
                    "type": type, 
                    "province": province, 
                    "licenseNo": licenseNo 
                }));

            function getParameterByName(name, url = window.location.href) {
                name = name.replace(/[\[\]]/g, '\\$&');
                var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                    results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';
                return decodeURIComponent(results[2].replace(/\+/g, ' '));
            }

            var redirect = getParameterByName('redirect')
            if(redirect){
                return window.location.href = redirect;
            }else {
                return window.location.href = 'home.html';
            }

        });
    });
</script>
{% endblock %}