{% extends "layout.nunjucks" %} {% block title %} Évolution du traitement D+C | Vyxeos {% endblock %} {% block content %}
<!--
  

-->
<style>
    .secondary-color {
        color: #72267A;
    }
    .gray-border {
        border-top: #CACACA 5px solid;
        border-bottom: #CACACA 5px solid;
    }
    .callout {
        font-size:24px;
    }
</style>



<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>LA CHIMIOTHÉRAPIE D’ASSOCIATION EST UTILISÉE COMME
TRAITEMENT INTENSIF POUR LES ADULTES ATTEINTS DE LMA À
HAUT RISQUE DEPUIS DES DÉCENNIES<sup>*1,2</sup></h1>
         
        </div>
    </div>

    <div class="row my-5">
        <div class="col-12">
            <img  class="img-fluid"  src="/assets/img/fr/combination_therapy.png" alt="Évolution du traitement par la daunorubicine et la cytarabine" />
        </div>
    </div>

    <div class="row my-5">
        <div class="col-12 col-xl-6">
            <p><strong>Au début des années 2000,</strong><br />
               le développement de VYXEOS a commencé au
Canada – dans une préparation liposomale conçue
pour optimiser le rapport synergique entre la
daunorubicine et la cytarabine<sup>1</sup>.</p>
        </div>
        <div class="col-12 col-xl-6 gray-border secondary-color  ">
            <p class="my-3 callout"><strong>VYXEOS OFFRE UN TRAITEMENT INTENSIF AUX
PATIENTS ADULTES À HAUT RISQUE ATTEINTS
DE LMAs QUI, JUSQU’À PRÉSENT, DISPOSAIENT
DE PEU D’OPTIONS<sup>*‡4</sup>.</strong></p>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <p class="footnote mb-2">
                <sup>*</sup> LMA à haut risque définie comme étant une LMA-t ou une LMA-CM.
            </p>
            <p class="footnote mb-2">
                <sup>&dagger;</sup> La chimiothérapie classique comprend le protocole 7 + 3/5 + 2 pour l’induction et la consolidation, avec la cytarabine et la daunorubicine<sup>3</sup>.
            </p>
            <p class="footnote mb-2">
                <sup>&ddagger;</sup> LMAs définie comme étant une LMA survenant à la suite d’un traitement et (ou) d’antécédents de maladie myéloïde, p. ex. SMD ou SMP, à l’exclusion de la leucémie myéloïde chronique, peu
importe si un traitement cytotoxique a déjà été administré pour ces affections. Cela inclut les sous-types LMA-t, LMA-CM et LMA-LMMC<sup>5,6</sup>.
            </p>

            <p class="footnote mb-2">
             <strong>Références :</strong> 
             <strong>1.</strong> Tolcher AW, Mayer LD. Improving combination cancer therapy: the combiplex<sup>&reg;</sup>  development platform. <em>Future Oncol</em> 2018;14:1317-32. 
             <strong>2.</strong> Tardi P, Johnstone S, Harasym N <em>et al</em>. In vivo maintenance of synergistic cytarabine:daunorubicin ratios greatly enhances therapeutic efficacy. <em>Leuk Res</em> 2009;33:129-39. 
             <strong>3.</strong> Lancet JE, Uy GL, Cortes JE <em>et al</em>. CPX-351 (cytarabine and daunorubicin) liposome for injection versus conventional cytarabine plus daunorubicin in older patients with newly diagnosed secondary acute myeloid leukemia. <em>J Clin Oncol</em> 2018;36:2684-2692.
            <strong>4.</strong> Talati C, Lancet JE. CPX-351: changing the landscape of treatment for patients with secondary acute myeloid leukemia. <em>Future Oncol</em> 2018;14:1147-1154. 
            <strong>5.</strong> Granfeldt Østgård LS, Medeiros BC, Sengeløv H <em>et al</em>. Epidemiology and clinical significance of secondary and therapy-related acute myeloid leukemia: A national population-based cohort study. <em>J Clin Oncol</em> 2015;33:3641-9. 
            <strong>6.</strong> Cité en référence avec l’autorisation des NCCN Clinical Practice Guidelines in Oncology (NCCN Guidelines<sup>&reg;</sup>), Acute Myeloid Leukemia, V.2.2021. &copy; National Comprehensive Cancer Network, Inc. 2021. Tous droits réservés. Consulté en mars 2021. Pour obtenir la version la plus récente et complète des lignes directrices, consultez le site <a href="https://NCCN.org">NCCN.org</a>.
              
            </p>
            </p>
        </div>
    </div>
</div>

{% endblock %}