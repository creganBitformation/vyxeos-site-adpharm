{% extends "layout.nunjucks" %}
{% from "macros.nunjucks" import scroll_image %}
{% block title %}EFFICACY RESULTS | Vyxeos {% endblock%}
l{% block content %}

<style>
    .heading {
        font-weight: bold;
        background-color: #72267A;
        color: #fff;
    }

    .sub-heading {
        font-weight: bold;
        vertical-align: text-top;
        background-color: #D5BED7;
        border: #FFF 1px solid;
    }

    .table-data {
        border: #72267A 1px solid;
    }

    .or-icon {
        background-color: #FFD000;
        outline: #fff 1px solid;
    }

    .dianostic {
        color: #fff;

        background-color: #72267A;
    }

    .icon {
        height: 80px;
    }

    ul.types {
        list-style: inside;
        color: #95205E;
    }

    ul.types>li {
        color: black;
    }

    h4 {
        color: #72267A;
    }

    .secondary-title {
        font-weight: bold;

    }
</style>



<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>EFFICACY RESULTS</h1>
            <h4 class="mt-3">VYXEOS DEMONSTRATED SUPERIORITY IN OVERALL SURVIVAL COMPARED TO CONVENTIONAL CHEMOTHERAPY IN PATIENTS
                AGED 60–75 WITH HIGH-RISK AML<sup>*&dagger;1</sup></h4>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-12">
            <p class="mb-0">PRIMARY ENDPOINT</p>
            <p class="secondary-title mb-2">OVERALL SURVIVAL IN THE ITT POPULATION<sup>1</sup></p>
        </div>
    </div>

    {{ scroll_image("/assets/img/en/vyexos_survival_1.png","Primary endpoint (overall survival) graph","","graph") }}


    {{ scroll_image("/assets/img/en/vyexos_survival_2.png","1- and 2-year survival landmarks","","chart") }}

    <div class="row">
        <div class="col-12">
            <h4 class="mt-3" >PATIENTS TREATED WITH VYXEOS ACHIEVED SIGNIFICANTLY GREATER RESPONSE RATES VS
                CONVENTIONAL CHEMOTHERAPY<sup>*1</sup></h4>
            <p class="mb-0 mt-3">SECONDARY ENDPOINT</p>
            <p class="secondary-title mb-2">RESPONSE RATES IN PATIENTS TREATED WITH VYXEOS VS CONVENTIONAL
                CHEMOTHERAPY<sup>*1</sup></p>

            <img class="img img-graphic d-none d-xl-block" src="/assets/img/en/vyexos_survival_3.png" alt="Secondary endpoint (response rates) graph" />
            <img class="img-fluid img-graphic d-xl-none" src="/assets/img/en/vyexos_survival_3_mobile.png" alt="Secondary endpoint (response rates) graph" />

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h4 class="mt-3">LONGER OVERALL SURVIVAL WITHOUT HSCT</h4>
            <p>Median OS</p>

            <img class="img img-graphic d-none d-xl-block" src="/assets/img/en/vyexos_survival_4.png" alt="Median overall survival without HSCT data" />
            <img class="img-fluid img-graphic d-xl-none" src="/assets/img/en/vyexos_survival_4_mobile.png" alt="Median overall survival without HSCT data" />

        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <h4 class="mt-3">PATIENTS TREATED WITH VYXEOS REACHED SIGNIFICANTLY LONGER EVENT-FREE SURVIVAL
                LANDMARKS VS CONVENTIONAL CHEMOTHERAPY<sup>*1</sup></h4>
            <p class="mb-0 mt-3">SECONDARY ENDPOINT</p>
            <p class="secondary-title mb-2">MEDIAN EVENT-FREE SURVIVAL IN PATIENTS TREATED WITH VYXEOS VS CONVENTIONAL
                CHEMOTHERAPY<sup>*1</sup></p>
            <figure class="figure">
                <img class="figure-img img-graphic img-fluid" src="/assets/img/en/vyexos_survival_5.png" alt="Median event-free survival data" />
                <figcaption class="figure-caption figure-caption-black">Adapted from the VYXEOS Product Monograph.
                </figcaption>
            </figure>


        </div>
    </div>
    <div class="row">
        <div class="col-12">

            <img class="img img-graphic d-none d-xl-block" src="/assets/img/en/vyexos_survival_6.png" alt="30- and 60-day mortality rates" />
            <img class="img-fluid img-graphic d-xl-none" src="/assets/img/en/vyexos_survival_6_mobile.png" alt="30- and 60-day mortality rates" />

        </div>
    </div>
    <div class="row">
        <div class="col-12">







            <p class="footnote mb-2">
                ITT: Intent-to-treat.
            </p>
            <p class="footnote mb-2">
                CI: Confidence interval.
            </p>
            <p class="footnote mb-2">
                HR: Hazard ratio.
            </p>
            <p class="footnote mb-2">
                HSCT: Hematopoietic stem cell transplantation.
            </p>
            <p class="footnote mb-2">
                CRi: Complete response with incomplete platelet or neutrophil recovery.
            </p>
            <p class="footnote mb-2">
                OR: Odds ratio.
            </p>
            <p class="footnote mb-2">
                <sup>*</sup> Conventional chemotherapy includes the 7+3/5+2 regimen for induction and consolidation,
                using cytarabine and daunorubicin.<sup>2</sup>
            </p>
            <p class="footnote mb-2">
                <sup>&dagger;</sup> High-risk AML defined as t-AML or AML-MRC.
            </p>

            <p class="footnote mb-2">
                <strong>REFERENCES:</strong> <strong>1.</strong> VYXEOS<sup>&reg;</sup> Product Monograph, Jazz
                Pharmaceuticals Canada, Inc., April 28, 2021.
                <strong>2.</strong> Lancet JE, Uy GL, Cortes JE, <em>et al</em>. CPX-351 (cytarabine and daunorubicin)
                liposome for injection versus conventional cytarabine plus daunorubicin in older patients with newly
                diagnosed secondary acute myeloid leukemia. <em>J Clin Oncol</em> 2018;36:2684-2692.
                <strong>3.</strong> Lin TL, Rizzieri DA,Ryan DH, <em>et al</em>. Older adults with newly diagnosed
                high-risk/secondary AML who achieved remission with CPX-351: phase 3 post hoc analyses. <em>Blood
                    Adv</em> 2021;5(6):1719-1728.
                <strong>4.</strong> Jazz Pharmaceuticals. Data on File. Early mortality.
            </p>

            </p>
        </div>
    </div>
</div>

{% endblock %}