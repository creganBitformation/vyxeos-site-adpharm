{% extends "layout.nunjucks" %} {% block title %} Clinical Use | Vyxeos {% endblock %} {% block content %}

<style>
    h3.usage{
        font-weight: bold;
        text-decoration: underline;
    }
    .alert-clinical { 
        border: 1px black solid;
    }
    .serious {
        font-weight: bold;
        text-decoration: underline;
    }
</style>



<div class="container">
    <div class="row">
        <div class="col-6">
            <h3 class="usage">Clinical use:</h3>
            <p><strong>Pediatrics (&lt;18 years):</strong> The safety and effectiveness of VYXEOS in the
                treatment of newly diagnosed t-AML and AML-MRC has not been established
                in children and adolescent patients under 18 years of age.</p>
            <p><strong>Geriatrics (&ge;65 years of age):</strong> No significant differences in safety were
                observed in patients aged 65 or older.</p>

            <div class="alert alert-clinical" role="alert">
                <span class="serious">Serious warnings and precautions:</span> VYXEOS has different dosage
                recommendations than daunorubicin hydrochloride injection, cytarabine
                injection, daunorubicin citrate liposome injection, and cytarabine
                liposome injection. Verify drug name and dose prior to preparation and
                administration to avoid dosing errors.
            </div>
            <h3 class="usage">Relevant warnings and precautions:</h3>
            <ul>
                <li>VYXEOS must not be substituted or interchanged with other daunorubicin
                    and/or cytarabine-containing products</li>
                <li>Tissue necrosis</li>
                <li>Cardiotoxicity is a known risk of anthracycline treatment</li>
                <li>Driving and operating machinery</li>
                <li>VYXEOS should be used in patients with a history of Wilson’s disease or other
                    copper-related disorder only if the benefits outweigh the risks. Discontinue
                    VYXEOS in patients with signs or symptoms of acute copper toxicity</li>
                <li>Gastrointestinal mucositis and diarrhea</li>
                <li>Hematologic: Severe myelosuppression resulting in fatal infections and
                    hemorrhage has been reported in patients after administration with
                    VYXEOS. Patient blood counts should be regularly monitored during
                    VYXEOS treatment and appropriate supportive measures should be used for
                    clinical complications due to myelosuppression</li>
                <li>Hepatic or renal impairment may increase the risk of toxicity associated
                    with daunorubicin and cytarabine</li>
                <li>Serious hypersensitivity reactions, including anaphylactic reactions have
                    been reported with daunorubicin and cytarabine</li>
               
            </ul>

        </div>
        <div class="col-6">
            <ul>
                <li>Increased susceptibility to infections</li>
                <li>Cardiac function and blood uric acid levels should be closely monitored.
                    Appropriate therapy should be initiated if hyperuricemia develops</li>
                <li>Pregnancy: There are no data on the use of VYXEOS in pregnant women.
                    Patients should be advised to avoid becoming pregnant during VYXEOS
                    treatment. Male patients and women of childbearing potential must use
                    effective methods of contraception during treatment and for 6 months
                    following last dose of VYXEOS</li>
                <li>Male fertility may be compromised by treatment with VYXEOS according to
                    animal studies</li>
                <li>Nursing women should be advised not to breastfeed during treatment
                    with VYXEOS</li>
            </ul>
            <h3 class="usage">For more information:</h3>
            Consult the VYXEOS Product Monograph at: <a href="https://www.jazzpharma.com">www.jazzpharma.com</a> for
            important information relating to adverse reactions, drug interactions, and
            dosing information which has not been discussed in this piece.
            The Product Monograph is also available by calling our medical department
            at: <a href="tel:+18005205568">1-800-520-5568</a>.
        </div>
    </div>
</div>

{% endblock %}