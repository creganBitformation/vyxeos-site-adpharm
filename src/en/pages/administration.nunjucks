{% extends "layout.nunjucks" %} {% block title %} Prepration and Administration | Vyxeos {% endblock %} {% block content %}
<!--
  

-->
<style>
    .secondary-color {
        color: #72267A;
        font-weight:500;
    }

    .gray-border {
        border-top: #CACACA 5px solid;
        border-bottom: #CACACA 5px solid;
    }

    .primary {
        color: #fff;
        background-color: #72267A;
    }

    .secondary {
        color: #000;
        background-color: #D5BED7;
    }

    .empty {
        background-color: initial;

    }
    .icon {
        height: 120px;
    }
    .strong {
        font-weight: bold;
    }
    .heading {
        font-weight: bold;
        color: #72267A;
        font-size: larger;
    }
    .alert-clinical { 
        background-color: #5F267A;
        color: #fff;
        font-weight: bold;
    }

</style>



<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>PREPARATION AND ADMINISTRATION<sup>1</sup></h1>
        </div>
    </div>
   <div class="row">
        <div class="col-6 col-xl-4">
             <div class="text-center">
            <img class="icon my-3" src="/assets/img/en/administration_calculate.png" alt="math symbols" />
            </div>
            <p><span class="heading">Calculate</span> VYXEOS
                dose based on patient’s BSA
                and determine the number of
                vials required.</p>
        </div>
        <div class="col-6 col-xl-4">
         <div class="text-center">
            <img class="icon  my-3" src="/assets/img/en/administration_remove.png" alt="Fridge" />
            </div>
            <p><span class="heading">Remove</span>  the vials of VYXEOS from
                the refrigerator and <strong>equilibrate</strong> at room
                temperature for 30 minutes.</p>

            <h2 class="text-center"><span class="badge badge-pill badge-danger">DO NOT HEAT</span></h2>
        </div>
        <div class="col-6 col-xl-4">
         <div class="text-center">

            <img class="icon  my-3" src="/assets/img/en/administration_resonstutue.png" alt="Stop watch" />
            </div>
            <p><span class="heading">Reconstitute</span>  each vial with 19 mL of
                sterile water for injections using a 20 mL sterile
                syringe and start a 5-minute timer immediately.
                <strong>Carefully swirl the contents</strong> of the vial for
                5 minutes while gently inverting the vial every
                30 seconds.</p>
        </div>
        <div class="col-6 col-xl-4">
         <div class="text-center">
            <img class="icon  my-3" src="/assets/img/en/administration_rest.png" alt="Reconstituted product" />
            </div>
            <p><span class="heading">Rest</span> for 15 minutes after
                reconstitution. If the reconstituted
                product is not immediately diluted
                into an infusion bag, <strong>store</strong> in a
                refrigerator (2&deg;C to 8&deg;C) for up
                to 4 hours.</p>
                <h2 class="text-center"><span class="badge badge-pill badge-danger text-wrap">DO NOT VORTEX OR SHAKE&nbsp;VIGOROUSLY</span></h2>
        </div>
        <div class="col-6 col-xl-4">
             <div class="text-center">
            <img class="icon  my-3" src="/assets/img/en/administration_invert.png" alt="Circular arrows" />
            </div>
            <p><span class="heading">Gently invert</span> each vial
                5 times prior to withdrawing the
                concentrate for dilution.</p>

        </div>
        <div class="col-6 col-xl-4">
         <div class="text-center">
            <img class="icon  my-3" src="/assets/img/en/administration_withdraw.png" alt="Infusion bag" />
            </div>
            <p><span class="heading">Aseptically withdraw</span>
                the calculated volume of reconstituted VYXEOS
                from the vial(s) with a sterile syringe and
                transfer it to an infusion bag containing 500
                mL of sodium chloride 9 mg/mL (0.9%) solution
                for injection, or 5% glucose.
                <strong>Discard any unused portions.</strong></p>
                <p><span class="secondary-color">Gently invert </span>the bag to mix the solution.
                    If the diluted infusion solution is not used
                    immediately, store in a refrigerator
                    (2&deg;C to 8&deg;C) for up to 4 hours.</p>

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="alert alert-clinical " role="alert">
                <p class="p-0 mb-0 ml-2">VYXEOS is administered by intravenous infusion over a period of 90 minutes. Care should be taken to ensure
                    there is no extravasation to prevent the risk of tissue necrosis.</p>
            </div>
        </div> 
    </div>
    <div class="row mt-4">
            <a id="reconstitution-video"></a>
        <div class="col-12">
            <h4>RECONSTITUTING VYXEOS</h4>
                <p class="strong">Watch this demonstration on how VYXEOS can be reconstituted and prepared prior to dosing.</p>
            <div class="embed-responsive embed-responsive-16by9">
           <iframe title="Reconstituting VYXEOS video" src="https://player.vimeo.com/video/579951200?h=9bc1cdb1ef" allowfullscreen></iframe>
           </div>
           
        </div>
    </div>
    <div class="row">
        <div class="col-12">
           
            <p class="footnote mb-2">
                BSA: Body surface area.
            </p>
           

            <p class="footnote mb-2">
                <strong>REFERENCES:</strong> <strong>1.</strong> VYXEOS<sup>&reg;</sup> Product Monograph, Jazz
                Pharmaceuticals Canada, Inc., April 28, 2021. 
            </p>
            </p>
        </div>
    </div>
</div>

{% endblock %}